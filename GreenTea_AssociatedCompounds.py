import numpy
import pandas as pd
mycids = [] #empty list
r = pd.read_csv("mycids.csv") #read in cids
mycids = list(r["mycid"])#store cids
#print(myaids)

df = pd.read_csv("uniquecids.csv")#read in cids and compounds
#print(x)


idlist = list(df['mycid']) #store cids and compounds
#print(idlist)
df = df.fillna('')

fh = open("mycids.comp.csv","w")
print(len(mycids))
#looping through mycids and idlist to get associated compounds for the cids
for id in mycids:
  #print(id)
  if(id in idlist):
    index = idlist.index(id)
    target = df.iloc[index][1]
    #print(id)
    #if numpy.isnan(target) : target = ""
    #print(target)
    fh.write(str(id)+","+target+"\n")
  else:
    fh.write(str(id)+"\n")
fh.close()
