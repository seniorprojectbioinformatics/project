import numpy
import pandas as pd
mygis = []   #empty list
r = pd.read_csv("mygis.csv")     #read in gi numbers
mygis = list(r["mygi"]) #store csv values into mygis
#print(myaids)

df = pd.read_csv("uniquegis.csv")   #read in gi numbers and accession numbers
#print(x)


idlist = list(df['mygi'])   #store csv values into idlist
#print(idlist)
df = df.fillna('')

fh = open("mygi.acc.csv","w")
print(len(mygis))
#looping through mygis and idlist to determine the accession numbers and write out to file
for id in mygis:
  #print(id)
  if(id in idlist):
    index = idlist.index(id)
    target = df.iloc[index][1]
    #print(id)
    #if numpy.isnan(target) : target = ""
    #print(target)
    fh.write(str(id)+","+target+"\n")
  else:
    fh.write(str(id)+"\n")
fh.close()
