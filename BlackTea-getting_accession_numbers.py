import numpy
import pandas as pd
mybgis = []   # Empty list
r = pd.read_csv("mybgis.csv")  #Read in csv of gi's numbers
mybgis = list(r["mybgi"])   # Store values into mybgis
#print(myaids)

df = pd.read_csv("uniquebgis.csv")  #read in csv of gis and accession numbers
#print(x)


idlist = list(df['mybgi'])   #store those values into idlist
#print(idlist)
df = df.fillna('')

fh = open("mybgi.acc.csv","w")
print(len(mybgis))
#loop through mybgis to find their specific accession in the idlist and write out to a file
for id in mybgis:
  #print(id)
  if(id in idlist):
    index = idlist.index(id)
    target = df.iloc[index][1]
    #print(id)
    #if numpy.isnan(target) : target = ""
    #print(target)
    fh.write(str(id)+","+target+"\n")
  else:
    fh.write(str(id)+"\n")
fh.close()
