import numpy
import pandas as pd
myaccs = []    #empty list
r = pd.read_csv("myaccs.csv")  #read in csv of accession numbers
myaccs = r["myacc"]   #Store csv values in myaccs
#Reformat myaccs
z = myaccs.str.split(".", expand=True)
myaccs = z.drop(z.columns[[1]], axis=1)
myaccs = z[z.columns[0]].tolist()

#print(z)
print(myaccs)

df = pd.read_csv("uniqueaccs.csv")   #read in csv of accession numbers and gene id
#print(x)


idlist = list(df['myacc'])   #store csv values into idlist
#print(idlist)
df = df.fillna('')

fh = open("myacc.geneid.csv","w")
print(len(myaccs))
#looping through myaccs and idlist to get the associated gene ids for the accession numbers
for id in myaccs:
  #print(id)
  if(id in idlist):
    index = idlist.index(id)
    target = df.iloc[index][1]
    #print(id)
    #if numpy.isnan(target) : target = ""
    #print(target)
    fh.write(str(id)+","+target+"\n")
  else:
    fh.write(str(id)+"\n")
fh.close()