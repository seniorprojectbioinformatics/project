import numpy
import pandas as pd

# reading csv file f
data = pd.read_csv("gi_g.csv")

# dropping null value columns to avoid errors
data.dropna(inplace=True)


# data frame with split value columns
new = data["Target"].str.split("gi:", n=1, expand=True)

# making seperate name column from data frame
data["Name"] = new[0]

# making seperate gi values column from data frame
data["GI"] = new[1]

# Dropping old Name columns
data.drop(columns=["Name"], inplace=True)
#have gi numbers and target proteins
data.to_csv("mygis.targ.csv",sep=',')
